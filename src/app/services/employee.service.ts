import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';


const endpoint = 'https://lab-angular-springboot-73b91.appspot.com/employees';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  getEmployees(): Observable<any> {
    return this.http.get(endpoint).pipe(
      map(this.extractData));
  }

  getEmployee(id): Observable<any> {
    return this.http.get(endpoint + '/' + id).pipe(
      map(this.extractData));
  }

  addEmployee(employee): Observable<any> {
    console.log(employee);
    return this.http.post<any>(endpoint, JSON.stringify(employee), httpOptions).pipe(
      tap((employee) => console.log(`added employee w/ id=${employee.id}`)),
      catchError(this.handleError<any>('addProduct'))
    );
  }

  updateEmployee(id, employee): Observable<any> {
    return this.http.put(endpoint + '/' + id, JSON.stringify(employee), httpOptions).pipe(
      tap(_ => console.log(`updated employee id=${id}`)),
      catchError(this.handleError<any>('updateProduct'))
    );
  }

  deleteEmployee(id): Observable<any> {
    return this.http.delete<any>(endpoint + '/' + id, httpOptions).pipe(
      tap(_ => console.log(`deleted employee id=${id}`)),
      catchError(this.handleError<any>('deleteProduct'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

}
