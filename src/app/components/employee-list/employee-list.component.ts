import { EmployeeService } from './../../services/employee.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'Firstname', 'Lastname', 'Age', 'Email'];
  dataSource: any = [];

  animal: string;
  name: string;


  constructor(public employeeService: EmployeeService) { }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees() {
    this.employeeService.getEmployees().subscribe((data: {}) => {
      console.log(data);
      this.dataSource = data;
    });
  }

}
