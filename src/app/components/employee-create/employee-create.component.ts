import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/services/employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeRequest } from 'src/app/models/employee';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.scss']
})
export class EmployeeCreateComponent implements OnInit {

  public options: FormGroup;
  public addEmployeeForm: FormGroup;

  employeeRequest: EmployeeRequest;

  constructor(
    fb: FormBuilder,
    public employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.options = fb.group({
      hideRequired: false,
      floatLabel: 'auto',
    });
  }

  ngOnInit() {
    this.addEmployeeForm = new FormGroup({
      firstNameInput: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      lastNameInput: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      ageInput: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      emailInput: new FormControl('', [Validators.required, Validators.maxLength(30)]),
    });
  }

  createEmployee() {
    console.log(EmployeeCreateComponent.name + ' createEmployee');

    this.employeeRequest = {
      first_name: this.firstNameInput.value,
      last_name: this.lastNameInput.value,
      age: this.ageInput.value,
      email: this.emailInput.value,
    };



    this.employeeService.addEmployee(this.employeeRequest).subscribe((result) => {
      this.router.navigate(['/employees']);
    }, (err) => {
      console.log(err);
    });
  }

  onCancel() {

  }

  onSave() {

    console.log(EmployeeCreateComponent.name + ' onSave');

  }


  get firstNameInput() {
    return this.addEmployeeForm.controls.firstNameInput;
  }

  get lastNameInput() {
    return this.addEmployeeForm.controls.lastNameInput;
  }

  get ageInput() {
    return this.addEmployeeForm.controls.ageInput;
  }

  get emailInput() {
    return this.addEmployeeForm.controls.emailInput;
  }



}
