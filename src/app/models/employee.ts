export interface EmployeeRequest {
    id?: number;
    first_name: string;
    last_name: string;
    age: string;
    email: string;
}
